                                        DOCUMENTATION
Python Version 3

Main Function To Be Executed For User-Bot Interaction:  `parseInputForBot()`

How To Run Script On Terminal:
1. Change Directory to Current Directory Where all script file and input files are kept.
2. Just Run: `python parseJsonBot.py`
3. It Will Ask You To enter value either 1 or 2:
    --- 1 for checking user-bot interaction in respective of
        `assignment_1_input_1.json` file.
    --- 2 is in respective for `assignment_1_input_2.json`

4. Output of this interaction will be stored in json files.
    --- For First Input File: Output file = `assignment_1_response_1_aman.json`
    --- For Second Input File: Output file = `assignment_1_response_2_aman.json`

Note:
---All Variables Value are stored in dictionary named `inputVariables`
---Since I have used Python 3 so few changes were required in second input JSON file
because Python 3 does not support them.
Changes:
1. `range` in place of `xrange`
2. In object
{
            "calculated_variable":"True",
            "formula":"[map(int, i.split()) for i in row]",
            "var":"matrix"
},

row is used but it should be rows because input is stored in rows variable.

One Part is Incompleted:
  --I could not complete the `list_length` implementation because I tried to parse
  respective data but not able to Evaluated.
  --eval() was throwing error. I could not troubleshoot it.


---- You can create Html Documentation to See readme content on browser: Just run `htmlDoc.py` script!


Submitted By:
Aman Kaushal
Contact: 8989673137
Email: kaushalaman02@gmail.com
https://www.linkedin.com/in/amankaushaliiitm/
