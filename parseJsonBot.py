#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    __author__ = "Aman Kaushal"
    __email__ = "kaushalaman02@gmail.com"
    __status__ = "Production"
    __date__ = "21-08-2017 12:00:00"
"""
import json

"""
    Function for To Create Quick Replies Array from Given Options
"""
def createQuickReplies(optionsArray):
    tempArray = []
    for val in optionsArray:
        tempObj = {}
        if type(val) is str:
            tempObj["content_type"] = "text"
        elif type(val) in [int, float]:
            tempObj["content_type"] = "number"
        else:
            tempObj["content_type"] = "object"
        tempObj["title"] = "Object" if tempObj["content_type"] == "object" else str(val).capitalize()
        tempObj["payload"] = val.lower() if type(val) is str else val
        tempArray.append(tempObj)
    return tempArray

"""
    Function for To Evaluate Conditions given in object based on stored variables values
"""
def evaluateConditions(conditionArray, inputVariables):
    try:
        flag = False
        orJoinedStr = ""
        for i in conditionArray:
            if len(orJoinedStr):
                orJoinedStr += " or "
            andJoinedStr = ""
            for j in i:
                j = str(j)
                if len(andJoinedStr):
                    andJoinedStr += " and "
                for attr, value in inputVariables.items():
                    if attr in j:
                        flag = True
                        if type(value) is str:
                            j = j.replace(attr,"\'"+str(value)+"\'")
                        else:
                            j = j.replace(attr,str(value))
                        andJoinedStr += j
                        break
                if not flag:
                    andJoinedStr += j
                flag = False
            orJoinedStr += str((andJoinedStr))
        return eval(orJoinedStr)
    except:
        return False

"""
  Recursive Functions for Checking Enterd Value from User should be belong
  from given options by BOT
"""
def checkInputValueFromOptions(optionsArr, inputValue, question):
    optionsArr = [k.lower() if type(k) is str else k for k in optionsArr]
    value = inputValue.lower() if type(inputValue) is str else inputValue
    if value in optionsArr:
        return value
    else:
        inputValue = input("Please enter from values of Options: \n"+ question+":\t")
        return checkInputValueFromOptions(optionsArr, inputValue, question)


"""
  Recusrsive Function for Block User and ask same question again if entered input is not Valid
"""
def askQuestionAgainOnFailCondition(conditionArray, inputVariables, inputQuestion, val, tempArr):
    if evaluateConditions(conditionArray, inputVariables):
        x = input(inputQuestion) or ""
        inputVariables[val] = x
        tempArr.append(x)
        return askQuestionAgainOnFailCondition(conditionArray, inputVariables, inputQuestion, val, tempArr)
    else:
        return (tempArr[len(tempArr)-1] if len(tempArr) else inputVariables[val])

"""
  Function for Checking whether 'str' contains ALL of the chars in 'set
"""
def containsAll(str, set):
    return 0 not in [c in str for c in set]

"""
Main Function for Parsing Handling User Input and Bot interaction with user
"""
def parseInputForBot(data):
    questionArray       = []
    firstOutputJson     = {}
    secondOutputJson    = {}
    if data['questions'] and len(data['questions']):
        questionArray = data['questions']
        j = 0
        flag = False
        inputVariables = {}
        stage = ""
        for i in questionArray:
            if not flag and 'calculated_variable' not in i:
                j = j+1
                stage = "stage"+str(j)

            """
                list_length part is incompleted for second input file
                Tried To parse data but not able to Evaluated
            """
            if 'instruction' in i and 'list_length' in i:
                flag = True
                for g in range(int(i['list_length'])):
                    tempObj = {}
                    tempObj["message"] = {}
                    insTempArr = []
                    try:
                        if 'instruction_var' in i:
                            insTempArr = ["" if z in inputVariables else "" for z in i['instruction_var']]
                        tempObj["message"]["text"] = i['instruction'] % tuple(insTempArr)
                    except:
                        tempObj["message"]["text"] = i['instruction']
                    if stage not in firstOutputJson:
                        firstOutputJson[stage] = {}
                        firstOutputJson[stage]["Bot Says"] = []
                    firstOutputJson[stage]["Bot Says"].append(tempObj)
            elif 'instruction' in i:
                """
                Handled Case of Instruction Given By Bot
                """
                flag = True
                tempObj = {}
                tempObj["message"] = {}
                insTempArr = []
                try:
                    if 'instruction_var' in i:
                        insTempArr = [inputVariables[z] if inputVariables[z] else "" for z in i['instruction_var']]
                    tempObj["message"]["text"] = i['instruction'] % tuple(insTempArr)
                except:
                    tempObj["message"]["text"] = i['instruction']
                if stage not in firstOutputJson:
                    firstOutputJson[stage] = {}
                    firstOutputJson[stage]["Bot Says"] = []

                firstOutputJson[stage]["Bot Says"].append(tempObj)
            elif 'text' in i:
                """
                Handled Case of Text/Options/Conditions Given By Bot
                """
                boolRes = True
                flag = False
                tempObj = {}
                tempObj["message"] = {}
                tempObj["message"]["text"] = i['text']
                if 'conditions' in i:
                    boolRes = evaluateConditions(i['conditions'], inputVariables)
                if boolRes:
                    flagOptions = False
                    if stage not in firstOutputJson:
                        firstOutputJson[stage] = {}
                        firstOutputJson[stage]["Bot Says"] = []
                    if 'options' in i:
                        flagOptions = True
                        tempObj["message"]["quick_replies"] = createQuickReplies(i['options'])
                    firstOutputJson[stage]["Bot Says"].append(tempObj)
                    inputVariables[i['var']] = input(i['text']+":\t") or ""
                    if containsAll(i['var'], "[]"):
                        inputVariables[i['var'].split("[")[0]].append(inputVariables[i['var']])
                    inputQuestion = i['text']+":\t"
                    boolCondCheck = False
                    if 'conditions' in i:
                        boolCondCheck = askQuestionAgainOnFailCondition(i['conditions'], inputVariables, inputQuestion, i['var'], [])
                        inputVariables[i['var']] = boolCondCheck
                    if flagOptions:
                        inputVariables[i['var']] = checkInputValueFromOptions(i['options'], inputVariables[i['var']], i['text']+":\t")
                    firstOutputJson[stage]["User Says"] = inputVariables[i['var']].capitalize() if type(inputVariables[i['var']]) is str else inputVariables[i['var']]
                else:
                    j = j-1
            elif 'calculated_variable' in i and i['calculated_variable']:
                """
                Handled Case of Calculate Variables
                """
                flag = False if "instruction" in questionArray[questionArray.index(i)+1] else flag
                if i["formula"]:
                    inputVariables[i['var']] = eval(i["formula"], inputVariables)
    return firstOutputJson

if __name__ == "__main__":
    inp = input("Enter 1 for First input JSON file and Enter 2 for second input JSON file: ")
    if inp == "1":
        with open('assignment_1_input_1.json') as data_file:
            firstFileData = json.load(data_file)
        firstResultJson = parseInputForBot(firstFileData)
        with open('assignment_1_response_1_aman.json', 'w') as fp:
            json.dump(firstResultJson, fp, indent=2, sort_keys=True)
    else:
        with open('assignment_1_input_2.json') as data_file:
            secondFileData = json.load(data_file)
        firstResultJson = parseInputForBot(secondFileData)
        with open('assignment_1_response_2_aman.json', 'w') as fp:
            json.dump(firstResultJson, fp, indent=2, sort_keys=True)
