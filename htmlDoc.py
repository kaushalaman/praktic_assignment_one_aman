import os

if __name__ == "__main__":
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    FILE_PATH = os.path.join(BASE_DIR,"README.txt")
    contents = open(FILE_PATH,"r")
    with open("documentation.html", "w") as e:
        for lines in contents.readlines():
            e.write("<pre>" + lines + "</pre> <br>\n")
